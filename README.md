# ParaView Tutorial (also known as ParaView Self-direct Tutorial)

This tutorial has been moved to https://gitlab.kitware.com/paraview/paraview-docs .
and is now hosted on https://docs.paraview.org/en/latest/Tutorials/SelfDirectedTutorial/index.html .
Don't commit anything new here.
