Most of these Icons come from the ParaView source directory

```
paraview/Qt/Components/Resources/Icons
```

The files for icons that are in the ParaView toolbars start with `pq` in
the filename. To update them, it is best to delete the existing icon files
and add the new ones.

```sh
git rm pq*
cp ~/src/paraview/Qt/Components/Resources/Icons/pq* .
git add .
```
